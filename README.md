INTRODUCTION
------------

Alters in an option to every field formatter settings form to override the field
label. This allows different labels in different view modes.

This module intentionally works with field_display_label module. Together they
allow for the display label to be specified on the field edit form and the
override in the Manage Fields UI is optional.

REQUIREMENTS
------------

INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
